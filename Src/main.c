/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "../Inc/ringbuffer.h"
#include "../Inc/svvis_async.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
UART_HandleTypeDef huart1;

/* Definitions for mainTask */
osThreadId_t mainTaskHandle;
const osThreadAttr_t mainTask_attributes = {
  .name = "mainTask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};
/* Definitions for transmitTask */
osThreadId_t transmitTaskHandle;
const osThreadAttr_t transmitTask_attributes = {
  .name = "transmitTask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};
/* Definitions for receiveTask */
osThreadId_t receiveTaskHandle;
const osThreadAttr_t receiveTask_attributes = {
  .name = "receiveTask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};
/* Definitions for oscillator */
osTimerId_t oscillatorHandle;
const osTimerAttr_t oscillator_attributes = {
  .name = "oscillator"
};
/* Definitions for transmitSem */
osSemaphoreId_t transmitSemHandle;
const osSemaphoreAttr_t transmitSem_attributes = {
  .name = "transmitSem"
};
/* Definitions for receiveSem */
osSemaphoreId_t receiveSemHandle;
const osSemaphoreAttr_t receiveSem_attributes = {
  .name = "receiveSem"
};
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART1_UART_Init(void);
void StartMainTask(void *argument);
void StartTransmitTask(void *argument);
void StartReceiveTask(void *argument);
void oscillatorCallback(void *argument);

/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

// Lookup-Table to generate Sine wave
#define LUT_LEN 256
const int sin_lut[] = {
        50,51,52,54,55,56,57,59,
        60,61,62,63,65,66,67,68,
        69,70,71,72,74,75,76,77,
        78,79,80,81,82,83,84,84,
        85,86,87,88,89,89,90,91,
        92,92,93,94,94,95,95,96,
        96,97,97,97,98,98,99,99,
        99,99,99,100,100,100,100,100,
        100,100,100,100,100,100,99,99,
        99,99,99,98,98,97,97,97,
        96,96,95,95,94,94,93,92,
        92,91,90,89,89,88,87,86,
        85,84,84,83,82,81,80,79,
        78,77,76,75,74,72,71,70,
        69,68,67,66,65,63,62,61,
        60,59,57,56,55,54,52,51,
        50,49,48,46,45,44,43,41,
        40,39,38,37,35,34,33,32,
        31,30,29,28,26,25,24,23,
        22,21,20,19,18,17,16,16,
        15,14,13,12,11,11,10,9,
        8,8,7,6,6,5,5,4,
        4,3,3,3,2,2,1,1,
        1,1,1,0,0,0,0,0,
        0,0,0,0,0,0,1,1,
        1,1,1,2,2,3,3,3,
        4,4,5,5,6,6,7,8,
        8,9,10,11,11,12,13,14,
        15,16,16,17,18,19,20,21,
        22,23,24,25,26,28,29,30,
        31,32,33,34,35,37,38,39,
        40,41,43,44,45,46,48,49,
};

// get sine wave
// A = 100, DC-Offset= 0, Phase = 0 to 256
static int getsin(int phase) {
  return sin_lut[phase % LUT_LEN] - 50;
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */
  // init ringbuffer to empty
  rb_init(&transmitrb);
  rb_init(&receiverb);
  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* Init scheduler */
  osKernelInitialize();

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* Create the semaphores(s) */
  /* creation of transmitSem */
  transmitSemHandle = osSemaphoreNew(1, 1, &transmitSem_attributes);

  /* creation of receiveSem */
  receiveSemHandle = osSemaphoreNew(1, 1, &receiveSem_attributes);

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* Create the timer(s) */
  /* creation of oscillator */
  oscillatorHandle = osTimerNew(oscillatorCallback, osTimerPeriodic, NULL, &oscillator_attributes);

  /* USER CODE BEGIN RTOS_TIMERS */

  // start timer with 200tick period
  // timer thread priority was set to realtime in cubeMX
  // timing is therefore guaranteed
  osTimerStart(oscillatorHandle, 200);
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of mainTask */
  mainTaskHandle = osThreadNew(StartMainTask, NULL, &mainTask_attributes);

  /* creation of transmitTask */
  transmitTaskHandle = osThreadNew(StartTransmitTask, NULL, &transmitTask_attributes);

  /* creation of receiveTask */
  receiveTaskHandle = osThreadNew(StartReceiveTask, NULL, &receiveTask_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* Start scheduler */
  osKernelStart();
 
  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/* USER CODE BEGIN Header_StartMainTask */
/**
  * @brief  Function implementing the mainTask thread.
  * @param  argument: Not used 
  * @retval None
  */
/* USER CODE END Header_StartMainTask */
void StartMainTask(void *argument)
{
  /* USER CODE BEGIN 5 */
  /* Infinite loop */
  char buffer[50];

  for(;;)
  {
    // "Echo Loop"
    svvis_async_read_string(buffer, 50);
    svvis_async_send_string("You Wrote:");
    svvis_async_send_string(buffer);

    osDelay(100);
  }
  /* USER CODE END 5 */ 
}

/* USER CODE BEGIN Header_StartTransmitTask */
/**
* @brief Function implementing the transmitTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTransmitTask */
void StartTransmitTask(void *argument)
{
  /* USER CODE BEGIN StartTransmitTask */
  /* Infinite loop */

  // Buffer CANNOT be too big!
  // Otherwise Stack is immediately full and the function cannot execute!
  char buffer[50];
  int numOfBytes = 0;

  for (;;) {
    // transmit everything in the buffer
    osSemaphoreAcquire(transmitSemHandle, 0);
    numOfBytes = rb_flush(&transmitrb, buffer, 50);
    osSemaphoreRelease(transmitSemHandle);

    if (numOfBytes > 0) {
      HAL_UART_Transmit(&huart1, buffer, numOfBytes, HAL_MAX_DELAY);
    }

    osDelay(1);
  }
  /* USER CODE END StartTransmitTask */
}

/* USER CODE BEGIN Header_StartReceiveTask */
/**
* @brief Function implementing the receiveTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartReceiveTask */
void StartReceiveTask(void *argument)
{
  /* USER CODE BEGIN StartReceiveTask */
  char buffer[20];
  int readcount = 0;

  /* Infinite loop */
  for(;;) {
//    while(__HAL_UART_GET_FLAG(&huart1,UART_FLAG_RXNE) && (readcount < 20)) {
//      HAL_UART_Receive(&huart1, buffer+readcount, 1, HAL_MAX_DELAY);
//      readcount ++;
//    }

    while((huart1.Instance->SR & UART_FLAG_RXNE) && (readcount < 20)) {
      buffer[readcount++] = huart1.Instance->DR;
    }

    if(readcount > 0) {
      osSemaphoreAcquire(receiveSemHandle, 0);
      rb_write(&receiverb, buffer, readcount);
      osSemaphoreRelease(receiveSemHandle);
      readcount = 0;
    }

    if((huart1.Instance->SR & UART_FLAG_RXNE) == 0) {
      osDelay(0);
    }
  }
  /* USER CODE END StartReceiveTask */
}

/* oscillatorCallback function */
void oscillatorCallback(void *argument)
{
  /* USER CODE BEGIN oscillatorCallback */
  static int counter = 0;
  counter++;
//  svvis_async_send_string("hallo");
  svvis_async_send_short(1, getsin(counter));
  svvis_async_send_short(2, getsin(counter * 2));
  svvis_async_send_short(3, 2 * getsin(counter * 3));
  svvis_async_send_short(4, 0.5 * getsin(counter * 10));
  /* USER CODE END oscillatorCallback */
}

 /**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM4 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM4) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
