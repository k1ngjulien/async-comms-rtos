// Julian Kandlhofer, 24. 03. 2020
// Implementation of a standard Ringbuffer

#include "ringbuffer.h"

ringbuffer_type transmitrb;
ringbuffer_type receiverb;

void rb_init(ringbuffer_type* rb) {
  rb->size = 0;
  rb->writepos = 0;
  rb->readpos = 0;
}

int rb_write(ringbuffer_type* rb, char* data, int len) {
  // fail if not enough space
  if((RBUF_SIZE - rb->size) < len) {
    return 1;
  }

  for(int i = 0; i< len; i++) {
    rb->buffer[rb->writepos] = data[i];
    // advance write index circularly
    rb->writepos = (rb->writepos + 1) % RBUF_SIZE;
    rb->size++;
  }

  return 0;
}

int rb_read(ringbuffer_type* rb, char* data, int len) {
  // fail if not enough bytes
  if(rb->size < len) {
    return 1;
  }

  for(int i = 0; i < len; i++) {
    data[i] = rb->buffer[rb->readpos];
    // advance read index circularly
    rb->readpos = (rb->readpos + 1) % RBUF_SIZE;
    rb->size--;
  }

  return 0;
}

int rb_peek(ringbuffer_type* rb, char* data) {
  // check for data in buffer
  if(rb->size == 0) {
    return 1;
  }

  // send back current byte
  data[0] = rb->buffer[rb->readpos];
  return 0;
}

int rb_flush(ringbuffer_type* rb, char* data, int maxLen) {
  int numOfBytes = rb->size;

  if(numOfBytes > maxLen) {
    return -1;
  }

  // read entire buffer
  rb_read(rb, data, rb->size);

  return numOfBytes;
}
