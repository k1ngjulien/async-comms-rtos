// Julian Kandlhofer, 5BHEL
// 23.03.2020
// SvVis Async Implementation
// see header for more information

#include "../Inc/svvis_async.h"
#include "../Inc/ringbuffer.h"
#include <string.h>
#include "cmsis_os.h"

// semaphores guarding buffers
extern osSemaphoreId_t transmitSemHandle;
extern osSemaphoreId_t receiveSemHandle;

// static helpers for implementation
static void writetorb     (char* data, int len);
static void readfromrb    (char* data, int len);
static int  peektypefromrb();
static void waitforsize   (ringbuffer_type* rb, osSemaphoreId_t sem, int size);


void svvis_async_send_int(int channel, int value) {
  char data[5];

  // send channel as first byte
  data[0] = channel;

  // send int as next four bytes
  memcpy(data+1, (void*)&value, sizeof(int));

  writetorb(data, 1 + sizeof(int));
}

void svvis_async_send_short(int channel, short value) {
  char data[5];

  // send channel as first byte
  data[0] = 10 + channel;

  // send short as next two bytes
  memcpy(data+1, (void*)&value, sizeof(short));

  writetorb(data, 1 + sizeof(short));
}

void svvis_async_send_string(char* str) {
  // send channel as first byte
  char data = 10;

  writetorb(&data, 1);
  // send string
  writetorb(str, strlen(str) + 1);
}

void svvis_async_send_float(int channel, float value) {
  char data[5];
  // send channel as first byte
  data[0] = 20 + channel;
  // send float as next four bytes
  memcpy(data+1, (void*)&value, sizeof(float));

  writetorb(data, 1 + sizeof(float));
}

int svvis_async_read_int(int* data) {
  char buffer[5];
  int type = peektypefromrb();

  // check for correct datatype
  // int: 1 ... 9
  if ((type <= 0) || (type >= 10)) {
    return 1;
  }

  // read bytes from buffer
  readfromrb(buffer, 5);

  // convert bytes to int
  memcpy((void*)&data, buffer +1, 4);

  return 0;
}

int svvis_async_read_short(short* data) {
  char buffer[3];
  int type = peektypefromrb();

  // short: 11 ... 19
  if ((type <= 10) || (type >= 20)) {
    return 1;
  }

  readfromrb(buffer, 3);

  memcpy((void*)&data, buffer +1, 2);

  return 0;
}

int svvis_async_read_float(float* data) {
  char buffer[5];
  int type = peektypefromrb();

  // float: 21 ... 29
  if ((type <= 20) || (type >= 30)) {
    return 1;
  }

  readfromrb(buffer, 5);

  memcpy((void*)&data, buffer +1, 4);

  return 0;
}

int svvis_async_read_string(char* str, int maxLen) {
  char buffer[5];
  int count = 0;
  int type = peektypefromrb();

  // string: 10
  if (type != 10) {
    return 1;
  }

  // remove type
  readfromrb(str, 1);

  // read string until null byte
  do {
    readfromrb(str + count, 1);
    count ++;
  } while((str[count - 1] != '\0') && (count < maxLen-1));

  // set null terminator
  str[count] = '\0';

  return 0;
}



static void writetorb(char* data, int len) {
  // write data to the ringbuffer
  osSemaphoreAcquire(transmitSemHandle, 0);
  rb_write(&transmitrb, data, len);
  osSemaphoreRelease(transmitSemHandle);
}

// read from buffer and suspend while there's not enough data
static void readfromrb(char* data, int len) {
  waitforsize(&receiverb, receiveSemHandle, len);

  osSemaphoreAcquire(receiveSemHandle, 0);
  rb_read(&receiverb, data, len);
  osSemaphoreRelease(receiveSemHandle);
}

// get first byte of buffer but suspend if empty
static int peektypefromrb() {
  char type;

  waitforsize(&receiverb, receiveSemHandle, 1);

  osSemaphoreAcquire(receiveSemHandle, 0);
  // get one byte
  if(rb_peek(&receiverb, &type) == 1) {
    type = -1;
  }
  osSemaphoreRelease(receiveSemHandle);

  return type;
}

// waits for ringbuffer to have certain size
// suspends thread if size is to small to not block system
static void waitforsize(ringbuffer_type* rb, osSemaphoreId_t sem, int size) {
  int currSize;

  while(1){
    osSemaphoreAcquire(sem, 0);
    currSize = rb->size;
    osSemaphoreRelease(sem);

    if(currSize >= size) {
      break;
    }

    // let other threads do their work
    osDelay(1);
  }
}
