// Julian Kandlhofer, 5BHEL
// 23.03.2020
// SvVis Async Implementation
//  Implements the SvVis Protocol using read- and write-
//  ringbuffer.
//  Buffers must be read from and written to in separate
//  threads.
#ifndef SVVIS_ASYNC_H
#define SVVIS_ASYNC_H

/*
 * Send-Functions will write Data to transmit-buffer
 * to be sent out by transmit-thread
 */
void svvis_async_send_string(char* str);
void svvis_async_send_int   (int   channel, int   value);
void svvis_async_send_short (int   channel, short value);
void svvis_async_send_float (int   channel, float value);

/*
 * Read-Functions will suspend the current thread until
 * the value has been read.
 * Other threads will therefore not be affected.
 *
 * return:
 *  1 ... wrong type was read
 *  0 ... read was successful
 */
int svvis_async_read_int   (int*   data);
int svvis_async_read_short (short* data);
int svvis_async_read_float (float* data);
int svvis_async_read_string(char*  str, int maxLen);

#endif //SVVIS_ASYNC_H
