
// Julian Kandlhofer, 24. 03. 2020
// Standard Ringbuffer
// Operations are NOT thread safe and need to be protected by a mutex

#ifndef RINGBUFFER_H
#define RINGBUFFER_H

#define RBUF_SIZE 128

typedef struct {
    int readpos;
    int writepos;
    int size;
    char buffer[RBUF_SIZE];
}ringbuffer_type;

extern ringbuffer_type transmitrb;
extern ringbuffer_type receiverb;

// initializes empty ringbuffer
void rb_init(ringbuffer_type* rb);

// writes specified number of bytes to the ringbuffer
// return:
//    0 ... success
//    1 ... not enough space
int rb_write(ringbuffer_type* rb, char* data, int len);

// reads specified number from the ringbuffer
// return:
//    0 ... success
//    1 ... not enough bytes to read
int rb_read(ringbuffer_type* rb, char* data, int len);

// get first byte without moving read position
// return:
//    0 ... success
//    1 ... no data stored
int rb_peek(ringbuffer_type* rb, char* data);

// read entire buffer
// return:
//    -1  ... not enough space
//    >=0 ... number of bytes read
int rb_flush(ringbuffer_type* rb, char* data, int maxLen);


#endif //RINGBUFFER_H
